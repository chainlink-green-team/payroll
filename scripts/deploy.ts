import { ethers } from "hardhat"

async function main() {
    console.log("🕐 Deploying Payroll Contract")
    const owner = (await ethers.getSigners())[0]

    const contractFactory = await ethers.getContractFactory("Payroll")
    const deployedContract = await contractFactory.deploy()

    console.log(`✅ Contract Deployed Address: ${deployedContract.address}`)
    console.log(`🔑 Contract Owner: ${owner.address}`)
}

main().catch((error) => {
    console.error(error)
    process.exitCode = 1
})
