# Payroll Ethereum Backend

### Commit Pattern

```
add: when you put new code
update: when you refactor old code
fix: when you unbreak your code
```

**commit example:**

```
add: new function for calculate the sum of the values
```

```
update: package.json script, `yarn build` to `yarn b`
```

```
update: rename function `getCars` to `listOfCars`
```

```
fix: function that mint NFT
```
