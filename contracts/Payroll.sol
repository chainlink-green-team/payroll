// SPDX-License-Identifier: MIT
pragma solidity ^0.8.17;

import "solmate/src/auth/Owned.sol";

contract Payroll is Owned {
    //////////////////////////////////////////////////////////////////////////
    // VARIABLES
    /////////////////////////////////////////////////////////////////////////
    uint256 employeeCounter = 1;

    struct Employee {
        string name;
        uint256 salary;
        address wallet;
    }
    mapping(address => Employee) public employeeMap;

    //////////////////////////////////////////////////////////////////////////
    // EVENTS
    /////////////////////////////////////////////////////////////////////////
    event NewEmployee(string name, uint256 salary, address wallet);

    constructor() Owned(msg.sender) {}

    //////////////////////////////////////////////////////////////////////////
    // FUNTIONS
    /////////////////////////////////////////////////////////////////////////
    function employees() public pure returns (address[] memory) {
        address[] memory employeesArray;
        return employeesArray;
    }

    function addEmployee(
        string memory _name,
        uint256 _salary,
        address _wallet
    ) public {
        Employee storage newEmployee = employeeMap[_wallet];
        newEmployee.name = _name;
        newEmployee.wallet = _wallet;
        newEmployee.salary = _salary;

        emit NewEmployee(_name, _salary, _wallet);
    }

    function salary(address _address) public view returns (uint256) {
        return employeeMap[_address].salary;
    }

    function employeeData(address _address)
        public
        view
        returns (Employee memory)
    {
        return employeeMap[_address];
    }
}
