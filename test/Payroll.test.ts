import { SignerWithAddress } from "@nomiclabs/hardhat-ethers/signers"
import { Payroll__factory, Payroll } from "../typechain-types"
import hardhat, { ethers } from "hardhat"
import { expect } from "chai"

describe("Payroll", () => {
    /**
     * @DeclareWallet
     * - Owner
     * - Others
     */
    let owner: SignerWithAddress
    let others: SignerWithAddress[]

    /**
     * @DeclareSmartContracts
     * - Payroll
     */
    let Payroll: Payroll

    describe("Wallets", async () => {
        it("Create Wallets", async () => {
            ;[owner, ...others] = await ethers.getSigners()
        })
    })

    describe("Deploy", async () => {
        it("Deploy Payroll", async () => {
            const PayrollFactory: Payroll__factory =
                await hardhat.ethers.getContractFactory("Payroll")
            Payroll = await PayrollFactory.deploy()
        })
    })

    describe("Employees", async () => {
        // @issue: https://gitlab.com/chainlink-green-team/payroll/-/issues/2
        describe("the contract must provide a list of address are employees or admins for this screen works", async () => {
            it("Should return an empty list of employee addresses at the initial moment", async () => {
                const response = await Payroll.employees()
                expect(response).to.be.an("array").that.is.empty
            })
            it("Should return an address the is Owner of contract", async () => {
                const response = await Payroll.owner()
                expect(response).to.be.an("string").to.equal(owner.address)
            })

            it("It must be possible to transfer ownership to other address", async () => {
                await Payroll.setOwner(others[1].address)
                const response = await Payroll.owner()
                expect(response).to.be.an("string").to.equal(others[1].address)
            })
        })

        // @issue: https://gitlab.com/chainlink-green-team/payroll/-/issues/3
        describe("The contract must provide the data of a specific employee", async () => {
            it("Should emit an event when the Employee `Jair` is added", async () => {
                await expect(
                    Payroll.addEmployee("Jair", 2500, others[0].address)
                )
                    .to.emit(Payroll, "NewEmployee")
                    .withArgs("Jair", 2500, others[0].address)
            })
            it("Shoud return some data about Employee like: name, salary and address", async () => {
                const { name, salary, wallet } = await Payroll.employeeData(
                    others[0].address
                )
                expect(name).to.be.an("string").to.equal("Jair")
                expect(salary.toNumber()).to.be.an("number").to.equal(2500)
                expect(wallet).to.be.an("string").to.equal(others[0].address)
            })
        })
    })
})
